package com.jt.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
/**
 * 基于此全局过滤器对用户身份进行认证
 */
//@Component
public class AuthGlobalFilter implements GlobalFilter, Ordered {//Authentication(认证)
    /**
     * 此方法为一个处理请求的方法
     * @param exchange 基于此对象获取请求和响应
     * @param chain 过滤链对象
     * @return SpringWebFlux中的Mono对象(一个Publisher对象),
     * 是springframework5.0后推出的新特性,
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange,
                             GatewayFilterChain chain) {
        //1.获取请求和响应对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //2.获取请求中的数据
        String username=
        request.getQueryParams().getFirst("username");
        //3.对请求数据进行分析和处理.
        //3.1认证失败
        if(!"admin".equals(username)){
            System.out.println("此用户不存在");
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();//返回Mono对象,响应数据只有状态码,没有具体内容
        }
        //3.2认证成功(继续向后执行这个执行链-包含其它过滤器)
        return chain.filter(exchange);
    }
    @Override
    public int getOrder() {
        return 0;
    }
}
