package com.jt.callback;

import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.BlockRequestHandler;
import com.alibaba.fastjson.JSON;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Component
public class GatewayRequestBlockHandler implements BlockRequestHandler {
    @Override
    public Mono<ServerResponse> handleRequest(
            ServerWebExchange serverWebExchange,
            Throwable throwable) {
        Map<String,Object> map=new HashMap<>();
        map.put("state",429);
        map.put("message","two many request");
        String jsonStr= JSON.toJSONString(map);
//        return ServerResponse.ok()
//                .body(Mono.just(jsonStr), String.class);
        return ServerResponse.status(HttpStatus.TOO_MANY_REQUESTS)
                .contentType(MediaType.TEXT_PLAIN)
                .body(Mono.just(jsonStr), String.class);
    }
}
