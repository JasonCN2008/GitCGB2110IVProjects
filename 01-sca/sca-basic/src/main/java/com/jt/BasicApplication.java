package com.jt;

import com.example.Point;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

//@Import(Point.class) //可以直接通过import方式将类交给spring管理
@SpringBootApplication
public class BasicApplication {
    public static void main(String[] args) {
        SpringApplication.run(BasicApplication.class);
    }

    @Bean
    @ConditionalOnMissingBean
    public Point point(){
        System.out.println("point()");
        return new Point();
    }
}
