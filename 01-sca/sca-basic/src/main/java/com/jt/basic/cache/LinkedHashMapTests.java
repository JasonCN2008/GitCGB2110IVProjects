package com.jt.basic.cache;

import java.util.LinkedHashMap;
import java.util.Map;

public class LinkedHashMapTests {
    /**
     * LruCache实现(有界缓存)
     */
    static LinkedHashMap<String,Object> cache;
    public static void doLruCache(){
        //基于链表记录key的访问顺序
        cache=new LinkedHashMap<String,Object>(3,
                        0.75f,true){
                   //基于这个方法判断容器是否满了，是否要移除元素
                   //true表示要移除最近经常不访问的元素了
                    @Override
                    protected boolean removeEldestEntry(Map.Entry<String, Object> eldest) {
                        return cache.size()>3;
                    }
        };
        cache.put("C", 300);
        cache.put("D", 400);
        cache.put("A", 100);
        cache.get("C");
        cache.put("B", 200);
        System.out.println(cache);//A,C,B
    }
    /**
     * 记录元素的访问顺序
     */
    public static void doAccess(){
        //基于链表记录key的访问顺序
        LinkedHashMap<String,Object> map=
                new LinkedHashMap<>(3,
                        0.75f,true);
        map.put("C", 300);
        map.put("D", 400);
        map.put("A", 100);
        map.put("B", 200);
        System.out.println(map);// C D A B
        map.get("C");
        System.out.println(map);// D A B C
    }
    /**
     * 记录元素的添加顺序
     */
    public static void doInsert(){
        //HashMap<String,Object> map=new HashMap<>();
        //默认会基于链表记录key的添加顺序
        LinkedHashMap<String,Object> map=new LinkedHashMap<>();
        map.put("C", 300);
        map.put("D", 400);
        map.put("A", 100);
        map.put("B", 200);
        map.get("C");
        System.out.println(map);
    }
    public static void main(String[] args) {
        //记录插入顺序
        //doInsert();
        //记录访问顺序
        //doAccess();
        //简易lruCache分析
        doLruCache();
    }
}
