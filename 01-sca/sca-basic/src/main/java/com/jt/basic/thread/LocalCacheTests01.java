package com.jt.basic.thread;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class LocalCacheTests01 {
    private static CopyOnWriteArrayList<String> cache=
            new CopyOnWriteArrayList<>();
    static /*synchronized*/ List<String> getData(){
        if(cache.isEmpty()) {
            synchronized (LocalCacheTests01.class) {
                if (cache.isEmpty()) {
                    //从数据库查数据
                    System.out.println("load data from database");
                    List<String> data = Arrays.asList("A", "B", "C");
                    //将数据放入cache中
                    cache.addAll(data);
                }
            }
        }
        return cache;
    }
    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println(getData());
                }
         });
        Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(getData());
            }
        });
        Thread t3=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(getData());
            }
        });
        t1.start();
        t2.start();
        t3.start();
    }
}
