package com.jvm;
//JVM参数分析
//1)输出GC的详细信息 : -XX:+PrintGC
public class GcTests01 {
    public static void main(String[] args) {
        byte[] b1=new byte[1024];//1k
        byte[] b2=new byte[1024];//1k
        byte[] b3=new byte[1024];//1k
        byte[] b4=new byte[1024];//1k
        b1=null;
        b2=null;
        System.gc();//手动启动GC
    }
}
