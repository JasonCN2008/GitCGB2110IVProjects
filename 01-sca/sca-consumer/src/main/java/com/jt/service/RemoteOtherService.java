package com.jt.service;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "sca-provider",contextId = "remoteOtherService")
public interface RemoteOtherService {
    //.....
}
