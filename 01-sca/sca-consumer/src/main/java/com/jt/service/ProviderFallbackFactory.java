package com.jt.service;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class ProviderFallbackFactory implements FallbackFactory<RemoteProviderService> {
    @Override
    public RemoteProviderService create(Throwable throwable) {
//        return new RemoteProviderService() {
//            @Override
//            public String echoMessage(String string) {
//                //............
//                return "服务忙,稍等片刻再访问";
//            }
//        };
        return (string) ->{//JDK8 lamdba
                //............
                return "服务忙,稍等片刻再访问";
        };

    }
}
