package com.jt.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 接口作用就是定义规范,这里的接口用于声明
 * 你要调用哪个服务,要访问服务中的什么资源,
 * 问题:
 * 1)通过谁声明调用哪个服务? @FeignClient
 * 2)@FeignClient中Value属性的含义?(两层含义)
 * 2.1)你要访问远端服务的服务名
 * 2.2)当前接口对应的实现类交给spring管理时,Bean的名字,
 * 当然,假如不想让value属性的值作为bean对象的名字,可以
 * 通过contextId属性指定一个名字.
 *
 * fallbackFactory用于定义一个回调工厂,这个回调工厂用于创建一个远程服务接口
 * 对象,通过此对象处理远程服务不可用时出现的一些问题.(这叫有备无患)
 */
@FeignClient(value = "sca-provider",
             contextId = "remoteProviderService",
             fallbackFactory = ProviderFallbackFactory.class)
public interface RemoteProviderService {
     /**
      * 接口上声明了要调用的服务,方法上声明要访问服务中的
      * 哪个url(资源),但是具体怎么调用我们不用写?那谁写?(底层)
      * Feign:明修栈道暗度陈仓
      * @param string
      * @return
      */
     //以前:http://ip:port/provider/echo/abcd
     @GetMapping("/provider/echo/{string}")
     String echoMessage(@PathVariable("string") String string);

}
