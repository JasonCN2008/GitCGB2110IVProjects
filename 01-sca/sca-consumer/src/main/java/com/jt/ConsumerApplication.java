package com.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @EnableFeignClients 注解用于描述配置类,告诉系统底层,在服务启动时
 * 扫描使用@FeignClient注解描述的远程服务调用接口,并为这些接口创建实现类对象(代理对象)
 * ,然后在这样的对象内部,可以进行远程服务调用.
 */
@EnableFeignClients
@SpringBootApplication
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    /**
     * 我们通过此对象进行远程服务调用,也就是说去调用其它服务.
     * 此对象对基于HTTP协议进行服务调用的过程进行封装,提供
     * 了一些模板方法(get,post,put,delete,....),专门用于
     * 调用网络另一端的服务.
     */
    @Bean //bean的名字默认为方法名
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    /**
     * @LoadBalanced 注解描述RestTemplate对象时,是告诉底层为
     * RestTemplate对象注入一个负载均衡拦截器,当我们使用RestTemplate
     * 进行远程服务调用时,首先执行负载均衡拦截器中的方法,在方法内部,
     * 基于服务发现这种机制,从nacos获取服务实例,然后再进行远程调用.
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate loadBalancedRestTemplate(){
        return new RestTemplate();
    }

    /**
     * Java代码中指定负载均衡策略
      * @return
     */
//    @Bean
//    public IRule ribbonRule(){
//        return new RandomRule();
//    }

}
