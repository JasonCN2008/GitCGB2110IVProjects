package com.jt.controller;

import com.jt.service.RemoteProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeignConsumerController {

    @Autowired
    private RemoteProviderService remoteProviderService;
    //http://localhost:8090/consumer/echo/abcd
    @GetMapping("/consumer/echo/{msg}")
    public String doFeignEcho(@PathVariable("msg") String msg){
        return remoteProviderService.echoMessage(msg);
    }
}
