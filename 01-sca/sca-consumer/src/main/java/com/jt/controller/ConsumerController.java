package com.jt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerController {
    @Autowired
    private RestTemplate restTemplate;

//    @Autowired
//    public ConsumerController(RestTemplate restTemplate){
//        this.restTemplate=restTemplate;
//    }

    @Value("${server.port:8090}")
    private String serverPort;

    //http://localhost:8090/consumer/doRestEcho1
    @GetMapping("/consumer/doRestEcho1")
    public String doRestEcho1(){
        //在这里调用sca-provider服务
        //这里写的地址不正确会出现成调用404异常
        //String url="http://localhost:8081/provider/echo/8090";
        //return restTemplate.getForObject(url, String.class);

        String url="http://localhost:8081/provider/echo/{msg}";
        return restTemplate.getForObject(url, String.class,serverPort);
    }

    /**
     * 此对象负责实现对远程服务进行负载均衡方式的获取,
     * 底层默认基于Ribbon组件(负载均衡组件)中内置算法(轮询,随机,hash,...)获取某个服务实例.
     * 说明:这个对象是我们项目添加了spring cloud以后,系统启动时会自动创建这个对象,
     * 所以我们需要时,直接从spring容器获取即可.
     */
    @Autowired
    private LoadBalancerClient loadBalancerClient;

    /**
     * 负载均衡设计
     * @return
     */
    @GetMapping("/consumer/doRestEcho2")
    public String doRestEcho2(){
        //1.方案1:自定义负载均衡
        //定义所有服务的地址(当服务实例随着业务的变化,服务发生变化,这段代码就不够灵活了)
        //String url1="http://localhost:8081/provider/echo/{msg}";
        //String url2="http://localhost:8082/provider/echo/{msg}";
        //String[] urls={url1,url2};
        //从所有服务地址中随机选择其中的一个url
        //String url=urls[new Random().nextInt(urls.length)];
        //return restTemplate.getForObject(url, String.class,serverPort);

        //2.方案2:基于LoadBalancerClient对象实现负载均衡
        ServiceInstance serviceInstance =//ServiceInstance表示服务实例
        loadBalancerClient.choose("sca-provider");//serviceId为nacos中的服务名
        String ip=serviceInstance.getHost();
        int port=serviceInstance.getPort();
        //用"+"方式拼接字符串
        //String url="http://"+ip+":"+port+"/provider/echo/{msg}";
        //用占位符方式构建url字符串,%s表示字符串占位符
        String url=String.format("http://%s:%s/provider/echo/{msg}",ip,port);
        return restTemplate.getForObject(url, String.class,serverPort);
    }
    //Spring
    //IOC(资源整合,空手套白狼)目标
    //DI(依赖注入~兵马未动,粮草先行)手段
    @Autowired
    private RestTemplate loadBalancedRestTemplate;

    @GetMapping("/consumer/doRestEcho3")
    public String doRestEcho3(){
        String serviceId="sca-provider";//服务名
        String url=String.format("http://%s/provider/echo/{msg}",serviceId);
        //当在这里进行远程服务调用(RPC)是,底层会这个请求进行拦截,
        //然后基于LoadBalancerClient对象通过服务名获取具体的服务实例(ServiceInstance)
        return loadBalancedRestTemplate.getForObject(url, String.class,serverPort);
    }

}











