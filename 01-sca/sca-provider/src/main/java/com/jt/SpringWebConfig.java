package com.jt;

import com.jt.provider.interceptor.TimerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SpringWebConfig implements WebMvcConfigurer {
    /**
     * 注册拦截器
     * @param registry 负责进行拦截器注册的对象，这里会将你些的拦截器
     *                 放到一个容器中(例如list集合)。
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
           registry.addInterceptor(new TimerInterceptor())
                   .addPathPatterns("/provider/*")
                   .excludePathPatterns("/provider/sentinel03");//排除要拦截的路径

    }
}
