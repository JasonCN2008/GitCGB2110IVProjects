package com.jt.provider.service;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ResourceBlockHandler {

    /**
     * 限流后的异常处理方法,应用于@SentinelResource注解中,
     * 此方法在编写时有如下几个要求:
     * 1)方法修饰符为public
     * 2)必须为static方法
     * 3)返回值类型与@SentinelResource注解描述的方法相同
     * 4)参数列表要匹配,并且最后参数为BlockException
     * 5)方法名自己定义
     * @param ex
     * @return
     */
    public static String doHandle(BlockException ex){
         log.warn("blocked by sentinel");
         return "blocked by sentinel ......";
    }
    public static String doHandle(Integer id,BlockException ex){
        log.warn("blocked by sentinel");
        return "blocked by sentinel ......";
    }
}
