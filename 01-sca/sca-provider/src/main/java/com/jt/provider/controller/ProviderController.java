package com.jt.provider.controller;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 构建Controller对象,用于处理客户端请求
 */
@RestController
public class ProviderController {
    /**
     * 从项目配置文件中读取server.port的值,然后赋值给serverPort属性,
     * 冒号右边的8080为一个默认值,假如从配置文件读取不到server.port
     * ,此时会将默认值赋值给属性serverPort
     */
    @Value("${server.port:8080}")
    private String serverPort;

    //http://localhost:8081/provider/echo/client
    @GetMapping("/provider/echo/{msg}")
    public String doRestEcho1(@PathVariable("msg") String msg,
                              HttpServletRequest request) throws InterruptedException {
        //模拟耗时操作
        //Thread.sleep(5000);

        //获取某个请求头信息
        //String header = request.getHeader("X-Request-Foo");
        //System.out.println("X-Request-Foo="+header);

        //获取所有请求头信息
//        Enumeration<String> headerNames = request.getHeaderNames();
//        while(headerNames.hasMoreElements()){
//            String headerName = headerNames.nextElement();
//            String headerValue=request.getHeader(headerName);
//            System.out.println(headerName+":"+headerValue);
//        }
        return serverPort+" say hello "+msg;
    }
}
