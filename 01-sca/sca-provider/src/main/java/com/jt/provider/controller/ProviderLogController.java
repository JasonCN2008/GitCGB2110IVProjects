package com.jt.provider.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class ProviderLogController {
    public ProviderLogController(){
        System.out.println("===ProviderLogController()===");
    }
    /**
     * org.slf4j.Logger 是一组日志API规范,
     * 作者推出这组规范最主要的目的是挟天子以令诸侯,
     * 希望让这组接口成为日志门面对象(门面模式),当然,
     * 既然大家都这么玩,所以我们也建议,项目中所有日志相关API
     * 都使用:
     * import org.slf4j.Logger;
     * import org.slf4j.LoggerFactory;
     */
    private static final Logger log=
            LoggerFactory.getLogger(ProviderLogController.class);

    @GetMapping("/provider/log/doLog01")
    public String doLog01(){//日志级别 trace<debug<info<warn<error
        log.trace("===trace==");
        log.debug("===debug==");
        log.info("===info==");
        log.warn("===warn==");
        log.error("===error==");
        return "test log level";
    }
    @Value("${logging.level.com.jt:info}")
    private String logLevel;

    @GetMapping("/provider/log/doLog02")
    public String doLog02(){
        log.debug("===debug==");
        log.info("===info==");
        log.warn("===warn==");
        log.error("===error==");
        return "log level is "+logLevel;
    }
}
