package com.jt.provider.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

/**
 * 这个类可以理解为资源服务类
 */
@Service
public class ResourceService{
    /**
     *  @SentinelResource 注解描述方法时，
     *  此方法就是一个sentinel资源方法(就是基于sentinel进行限流访问的一个方法)。
     *  同时这个方法也是一个AOP中的切入点方法，这个方法执行时，会进行一些限流规则
     *  的应用。
     * @return
     */
    @SentinelResource(value="doGetResource",
                      blockHandlerClass = ResourceBlockHandler.class,
                      blockHandler = "doHandle")
    public String doGetResource(){
        return "select resource";
    }

    @SentinelResource(value="selectById",
                      blockHandlerClass = ResourceBlockHandler.class,
                      blockHandler = "doHandle")
    public String doGetResource(Integer id){
        return "select resource by "+id;
    }

}
