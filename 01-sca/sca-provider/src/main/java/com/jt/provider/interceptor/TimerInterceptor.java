package com.jt.provider.interceptor;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

/**
 * 定义一个时间访问拦截器：
 * 基于访问时间进行请求的放行和终止。
 */
public class TimerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("==preHandle==");
        LocalTime now = LocalTime.now();
        int hour = now.getHour();
        if(hour<8||hour>=23){
            throw new RuntimeException("请在规定时间访问[8~23]");
        }
        return true;//true表示放行，false表示请求到此结束，不再进行传递
    }
}
