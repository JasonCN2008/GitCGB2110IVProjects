package com.jt.provider.controller;
import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;

/**
 * 请求解析器,基于这个对象可以对你的请求进行解析,具体你要通过
 * 请求对象获取什么数据,由你的业务决定.
 */
@Component
public class DefaultRequestOriginParser implements RequestOriginParser {

    @Override
    public String parseOrigin(HttpServletRequest request) {
        //解析请求参数
        //String origin=request.getParameter("origin");
        //return origin;//系统底层会应用这个返回值与sentinel授权规则中的值进行一个比对.

        //解析请求头数据
        //String token=request.getHeader("token");
        //return token;

        //解析请求ip
        String ip=request.getRemoteAddr();
        System.out.println("ip="+ip);
        return ip;
    }

}
