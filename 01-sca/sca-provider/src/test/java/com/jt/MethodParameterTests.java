package com.jt;

import com.jt.provider.controller.ProviderController;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.PathVariable;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@SpringBootTest
public class MethodParameterTests {

    /**
     * 在这个测试方法中获取ProviderController类中的指定方法的参数名.
     */
    @Test
    void testMethodParameter01() throws NoSuchMethodException {
        //1.获取方法所在类的字节码对象
        Class<ProviderController> clazz = ProviderController.class;
        //2.获取方法对象
        Method targetMethod = clazz.getDeclaredMethod("doRestEcho1", String.class);
        //获取方法参数对象
        Parameter[] parameters = targetMethod.getParameters();
        for(Parameter p:parameters){
            System.out.println(p.getName());//arg0
            PathVariable anno = p.getAnnotation(PathVariable.class);
            if(anno!=null) {
                System.out.println(anno.value());
            }
        }
    }
}
