package com.jt.resource.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * 资源配置对象,需要配置:
 * 1)哪些资源允许匿名访问(浏览商品)
 * 2)哪些资源允许认证以后才可以访问.(购买商品)
 * 3)哪些资源认证后必须有权限才可以访问(购买特殊商品,回答某些人的问题).
 */
@Configuration
//开启资源访问控制(一般是资源服务器需要开启这个业务)
@EnableResourceServer
//开启方法上的权限检查,与方法上的@PreAuthorize注解配合使用
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceConfig extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        //super.configure(http);
        //1.关闭跨域攻击
        //http.csrf().disable();
        //2.定义资源访问规则
        http.authorizeRequests()
                 //配置需要认证以后才可以访问的资源
                .antMatchers("/resource/*").authenticated()
                 //其它资源则可以匿名访问
                .anyRequest().permitAll();
    }
}
