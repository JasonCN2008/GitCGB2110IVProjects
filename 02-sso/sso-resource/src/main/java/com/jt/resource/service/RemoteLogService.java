package com.jt.resource.service;

import com.jt.resource.pojo.Log;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 定义远程日志业务接口，通过此接口中的方法
 * 将日志对象传递给远端的sso-system服务
 */
@FeignClient(value = "sso-system",
        contextId = "remoteLogService")
public interface RemoteLogService {

    @PostMapping("/log")
    void insertLog(@RequestBody Log log);
}
