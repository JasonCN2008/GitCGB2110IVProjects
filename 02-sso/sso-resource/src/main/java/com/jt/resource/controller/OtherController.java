package com.jt.resource.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/other")
public class OtherController {
    /**
     * 希望此方法允许匿名访问
     * @return
     */
    @GetMapping
    public String doSelect(){
        return "select other resource ok";
    }
}
