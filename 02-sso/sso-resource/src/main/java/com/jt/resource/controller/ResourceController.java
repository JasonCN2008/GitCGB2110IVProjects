package com.jt.resource.controller;

import com.jt.resource.annotation.RequiredLog;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/resource")
@RestController
public class ResourceController {
    /**
     * @PreAuthorize 注解描述的方法为一个授权切入点方法,访问此方法时
     * ,底层会基于AOP方式进行权限检查,判定用户token的权限中是否包含
     * hasAuthority中指定的权限(例如sys:res:list)
     * @return
     */
    @RequiredLog(value="查询资源")
    @PreAuthorize("hasAuthority('sys:res:list')")
    @GetMapping
    public String doSelect(){
        return "select resource ok!";
    }
    @RequiredLog(value="创建资源")
    @PreAuthorize("hasAuthority('sys:res:create')")
    @PostMapping
    public String doCreate(){
        return "create resource ok";
    }

    @RequiredLog(value="更新资源")
    @PreAuthorize("hasAuthority('sys:res:update')")
    @PutMapping
    public String doUpdate(){
        return "update resource ok";
    }

    @PreAuthorize("hasAuthority('sys:res:delete')")
    @DeleteMapping
    public String doDelete(){
        return "delete resource ok";
    }
    /**
     * 登录后可以访问
     * @return
     */
    @GetMapping("/export")
    public String doExport(){
        return "export resource ok";
    }
}
