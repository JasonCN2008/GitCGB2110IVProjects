package com.jt.service;

import com.jt.system.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserServiceTests {
    @Autowired
    private UserService userService;
    @Test
    void testSelectUserPermissions(){
        List<String> permissions =
                userService.selectUserPermissions(1L);
        System.out.println(permissions);
    }
}
