package com.jt.dao;

import com.jt.system.dao.UserMapper;
import com.jt.system.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserMapperTests {
    @Autowired
    private UserMapper userMapper;//此变量指向一个代理对象
    @Test
    void testSelectUserByUsername(){
        //userMapper对象底层会通过SqlSessionTemplate对象执行与数据库的会话
        User user = userMapper.selectUserByUsername("admin");
        System.out.println(user);
    }

    @Test
    void testSelectUserPermissions(){
        List<String> pers =
                userMapper.selectUserPermissions(1L);
        System.out.println(pers);
    }
}
