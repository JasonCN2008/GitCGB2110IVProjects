package com.jt.dao;

import com.jt.system.dao.impl.UserMapperImpl;
import com.jt.system.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperImplTests {
    @Autowired
    private UserMapperImpl userMapperImpl;

    @Test
    void testSelectUserByUsername(){
        User user = userMapperImpl.selectUserByUsername("admin");
        System.out.println(user);
    }
}
