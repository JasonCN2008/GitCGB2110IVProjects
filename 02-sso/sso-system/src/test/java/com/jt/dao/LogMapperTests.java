package com.jt.dao;

import com.jt.system.dao.LogMapper;
import com.jt.system.pojo.Log;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class LogMapperTests {
    @Autowired
    private LogMapper logMapper;

    @Test
    void testInsert(){
        Log log=new Log();
        log.setIp("192.168.1.111");
        log.setUsername("admin");
        log.setCreatedTime(new Date());
        log.setOperation("select resource");
        log.setMethod("com.jt.resource.controller.ResourceController.doSelect");
        log.setParams("");
        log.setTime(100L);
        log.setStatus(1);
        logMapper.insert(log);
    }
}
