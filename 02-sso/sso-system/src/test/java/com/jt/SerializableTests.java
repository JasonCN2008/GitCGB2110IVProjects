package com.jt;

import com.jt.system.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;

@SpringBootTest
public class SerializableTests {
    /**
     * 序列化
     */
    @Test
    void testSerialize() throws IOException {
       //1.构建user对象
       User user=new User();
       user.setId(100L);
       user.setUsername("liuyujiang");
       user.setPassword("123456");
       //user.setStatus("1");
       //2.将此对象存储到一个文件
        ObjectOutputStream out=
                new ObjectOutputStream(
                        new FileOutputStream("user.txt"));
        out.writeObject(user);
        out.close();
        System.out.println("serialize ok");
    }

    /**
     * 反序列化
     */
    @Test
    void testDeserialize() throws IOException, ClassNotFoundException {
        ObjectInputStream ois=
           new ObjectInputStream(
                new FileInputStream("user.txt"));
        User user=(User)ois.readObject();
        System.out.println(user);
        ois.close();
    }
}
