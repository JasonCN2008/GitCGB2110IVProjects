package com.jt.system.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 基于此对象封装用户行为日志?
 * 谁在什么时间执行了什么操作,访问了什么方法,传递了什么参数,访问时长是多少.
 */
@Data
@TableName("tb_logs")
public class Log implements Serializable {
    private static final long serialVersionUID = 3054471551801044482L;
    @TableId(type = IdType.AUTO)
    private Long id;
    /**系统登录用户*/
    private String username;
    /**客户端的ip*/
    private String ip;
    /**资源访问时间,也是日志的创建时间*/
    @TableField("createdTime")
    private Date createdTime;
    /**具体操作名*/
    private String operation;
    /**目标方法(包名.类名.方法名)*/
    private String method;
    /**实际参数*/
    private String params;
    /**访问方法时的执行时长*/
    private Long time;
    /**访问状态:1表示ok,0表示error*/
    private Integer status;
    /**访问失败了的错误信息*/
    private String error;
}
