package com.jt.system.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 基于此对象存储用户信息
 * 说明：
 * Java中所有用于存储数据的对象，都建议：
 * 1)实现序列化接口Serializable
 * 2)手动添加序列化id
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 4831304712151465443L;
    private Long id;
    private String username;
    private String password;
    private String status;
}
