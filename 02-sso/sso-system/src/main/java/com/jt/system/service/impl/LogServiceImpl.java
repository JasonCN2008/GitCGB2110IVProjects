package com.jt.system.service.impl;

import com.jt.system.dao.LogMapper;
import com.jt.system.pojo.Log;
import com.jt.system.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 用户行为日志业务逻辑对象
 */
@Slf4j
@Service
public class LogServiceImpl implements LogService {

    //@Autowired
    private LogMapper logMapper;
    //@Autowired
    public LogServiceImpl(LogMapper logMapper){
        this.logMapper=logMapper;
    }

    /**
     *  此注解@Async描述的方法为一个异步切入点方法,
     *  @Async注解要想生效,还需要在启动类上添加
     *  @EnableAsync注解
     */
    @Async
    @Override
    public void insertLog(Log userLog) {
         log.debug("LogServiceImpl.insert.log.thread.name:{}",
                 Thread.currentThread().getName());
         logMapper.insert(userLog);
    }
}
