package com.jt.system.controller;

import com.jt.system.pojo.Log;
import com.jt.system.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/log")
public class LogController {
    private LogService logService;
    public LogController(LogService logService){
        this.logService=logService;
    }
    @PostMapping
    public void doInsertLog(@RequestBody Log userLog){
        log.debug("LogController.insert.log.thread.name:{}",
                Thread.currentThread().getName());
        logService.insertLog(userLog);
    }
}
