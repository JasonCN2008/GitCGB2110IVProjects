package com.jt.system.controller;

import com.jt.system.pojo.User;
import com.jt.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户请求和响应逻辑对象
 */
@RestController
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 基于用户名查询用户信息
     * @param username
     * @return
     */
    @GetMapping("/user/login/{username}")
    public User doSelectUserByUsername(
            @PathVariable("username") String username){
        return userService.selectUserByUsername(username);
    }

    /**
     * 基于用户id查询用户权限信息
     * @param userId
     * @return
     */
    @GetMapping("/user/permission/{userId}")
    public List<String> doSelectUserPermissions(
            @PathVariable("userId") Long userId){
        return userService.selectUserPermissions(userId);
    }
}
