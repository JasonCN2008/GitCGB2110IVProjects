package com.jt.system.dao.impl;

import com.jt.system.pojo.User;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Repository 注解是一个特殊@Component对象,
 * 一般用于描述数据层具体类(实现类)对象.
 */
@Repository
public class UserMapperImpl{
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    public User selectUserByUsername(String username){
        String namespace="com.jt.system.dao.UserMapper";
        String elementId="selectUserByUsername";
        String statement=String.format("%s.%s",namespace,elementId);
        return sqlSessionTemplate.selectOne(statement, username);
    }
}
