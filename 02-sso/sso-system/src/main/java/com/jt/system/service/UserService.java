package com.jt.system.service;

import com.jt.system.pojo.User;

import java.util.List;

/**
 * 定义用户业务逻辑接口
 */
public interface UserService {
    User selectUserByUsername(String username);
    List<String> selectUserPermissions(Long userId);
}
