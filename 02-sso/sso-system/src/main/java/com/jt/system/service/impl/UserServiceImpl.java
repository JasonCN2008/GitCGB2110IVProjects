package com.jt.system.service.impl;

import com.jt.system.dao.UserMapper;
import com.jt.system.pojo.User;
import com.jt.system.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Slf4j
@Service
@Transactional(readOnly = false) //底层AOP方式的事务控制
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    /**
     * 数据查询操作建议事务的readOnly属性值为true.
     * 目的是提高事务性能(例如取消了回滚日志的记录)
     * @param username
     * @return
     */
    @Transactional(readOnly = true)//事务切入点方法
    @Override
    public User selectUserByUsername(String username) {
        log.debug("username {}",username);
        return userMapper.selectUserByUsername(username);
    }

    @Cacheable(value = "userPermissions",key = "#userId")//缓存切入点
    @Transactional(readOnly = true)//事务切入点
    @Override
    public List<String> selectUserPermissions(Long userId) {
        log.debug("userId {}",userId);
        //.....

        return userMapper.selectUserPermissions(userId);
    }
    //.......
}
