package com.jt.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.system.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * 基于用户名查询用户信息
     * @param username
     * @return 封装了用户信息的对象
     */
//    @Select("select id,username,password,status " +
//            "from tb_users " +
//            "where username=#{username}")
    User selectUserByUsername(@Param("username") String username);

    /**
     * 查询用户权限信息
     * @param userId
     * @return
     */
    @Select("select distinct permission " +
            "from tb_user_roles ur join tb_role_menus rm " +
            "     on ur.role_id=rm.role_id " +
            "     join tb_menus m on rm.menu_id=m.id " +
            "where ur.user_id=#{userId}")
    List<String> selectUserPermissions(@Param("userId") Long userId);

}
