package com.jt.auth.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 定义User对象,用于接收远端服务(system服务)响应的用户信息
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = -7779976555863579590L;
    private Long id;
    private String username;
    private String password;
    private String status;
}
