package com.jt.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * 为系统提供密码加密对象,用户端输入的密码,
     * 提交到服务端,服务端要对这个密码进行加密,
     * 然后与数据库中的用户密码进行比对,所以
     * 这里需要提供一个加密对象,这里的BCryptPasswordEncoder
     * 内置了一种不可逆的密码加密算法.
     * 本次密码的加密我们就使用这个对象了.
     * @return
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * 这里写的AuthenticationManager主要服务于后面的OAuth2的配置.
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
