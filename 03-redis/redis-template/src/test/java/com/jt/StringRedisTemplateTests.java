package com.jt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;//Gson,fastJson
import com.jt.pojo.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
public class StringRedisTemplateTests {
    /**
     * StringRedisTemplate 是一个特殊的RedisTemplate对象,
     * 默认key/value都采用了String的序列化方式.
     */
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void testStringOper2() throws JsonProcessingException {
        ValueOperations<String, String> vo =
                stringRedisTemplate.opsForValue();
        Blog blog=new Blog();
        blog.setId(100L);
        //blog.setTitle("hello redis");
        vo.set("blog", new ObjectMapper().writeValueAsString(blog));
        String jsonStr = vo.get("blog");
        System.out.println(jsonStr);
    }
    @Test
    void testStringOper1(){
        ValueOperations<String, String> vo =
                stringRedisTemplate.opsForValue();
        vo.set("x", "100");
        vo.increment("x");
        String x = vo.get("x");
        System.out.println("x="+x);
    }

}
