package com.jt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.pojo.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.RedisSerializer;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.Map;

@SpringBootTest
public class RedisTemplateTests {
    /**
     * RedisTemplate对象定义了一些操作redis数据库的模板方法.
     * 基于RedisTemplate对象存储数据时,会对key和value基于jdk方式
     * 进行序列化.
     */
    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 清除数据库数据的测试
     */
    @Test
    void clearDb(){
        redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection redisConnection) throws DataAccessException {
                //redisConnection.flushDb();
                redisConnection.flushAll();
                return "flush ok";
            }
        });
    }

    /**
     * Hash类型数据读写测试 (HashOperations)
     */
    @Test
    void testHashOper01() throws JsonProcessingException {
       //1.获取操作redis数据库的对象
        ValueOperations valueOperations =
                redisTemplate.opsForValue();
        HashOperations hashOperations =
                redisTemplate.opsForHash();
        //2.读写数据
        Blog blog=new Blog();
        blog.setId(100L);
        blog.setTitle("study redis ");
        valueOperations.set("blog", blog);//序列化
        blog=(Blog)valueOperations.get("blog");//反序列化
        System.out.println("blog="+blog);
        //----
        //hashOperations.put("blogHash","id",blog.getId());
        //hashOperations.put("blogHash","title",blog.getTitle());
        ObjectMapper objectMapper = new ObjectMapper();//jackson中的api
        String jsonStr = objectMapper.writeValueAsString(blog);
        hashOperations.putAll("blogHash",
                objectMapper.readValue(jsonStr, Map.class));//序列化
        Map blogHash = hashOperations.entries("blogHash");//反序列化
        System.out.println("blogHash="+blogHash);
    }

    @Test
    void testStringOper02() {//定制key,value的序列化方式
        //1.获取Redis数据操作对象
        //1.1设置key的序列化方式
        redisTemplate.setKeySerializer(RedisSerializer.string());
        //1.2设置value的序列化方式
        redisTemplate.setValueSerializer(RedisSerializer.string());
        ValueOperations valueOperations =
                redisTemplate.opsForValue();
        //2.读写数据
        valueOperations.set("id", "100");
        valueOperations.set("title", "redis");
        valueOperations.set("content", "redis operation");
        Object title = valueOperations.get("title");
        Object content = valueOperations.get("content");
        System.out.println(title+"/"+content);
    }
    /**
     * 测试字符串操作
     */
    @Test
    void testStringOper01(){
         //1.获取Redis数据操作对象
        ValueOperations valueOperations =
                redisTemplate.opsForValue();
        //2.读写数据
        valueOperations.set("id", "100");
        //valueOperations.increment("id");//不可以
        Object id = valueOperations.get("id");
        System.out.println(id);
        Long x = valueOperations.increment("x");//可以,但是x在数据库中必须不存在
        System.out.println(x);
        //存储数据的同时设置key的有效时长
        valueOperations.set("y", 10, Duration.ofSeconds(10));
    }

    /**
     * 测试连通性
     */
    @Test
    void testGetConnection(){
        RedisConnection connection =
                //从连接工厂获取一个连接
                redisTemplate.getConnectionFactory()
                        .getConnection();
        String result = connection.ping();
        System.out.println(result);
        //我们获取了这个连接对应以后,其实可以使用这个对象
        //直接操作redis中数据,但是这个API直接使用,相对比较复杂
        //所以,redisTemplate对象在封装了基于这个连接操作redis的基本过程.
    }
}
