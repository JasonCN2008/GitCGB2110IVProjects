package com.jt.service;

import com.jt.pojo.Menu;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MenuServiceTests {
    @Autowired
    @Qualifier(value="defaultMenuService")
    private MenuService menuService;
    @Test
    void testSelectById(){
        Menu menu = menuService.selectById(4L);
        System.out.println(menu);
    }
    @Test
    void testInsertMenu(){
        Menu menu=new Menu();
        menu.setName("export");
        menu.setPermission("sys:res:export");
        menuService.insertMenu(menu);
    }
    @Test
    void testUpdateMenu(){
        Menu menu = menuService.selectById(4L);
        menu.setName("import resource");
        menuService.updateMenu(menu);
    }
}
