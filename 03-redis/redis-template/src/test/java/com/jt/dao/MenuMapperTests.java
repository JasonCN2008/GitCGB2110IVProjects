package com.jt.dao;

import com.jt.pojo.Menu;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MenuMapperTests {
    @Autowired
    private MenuMapper menuMapper;
    @Test
    void testSelectById(){
        Menu menu = menuMapper.selectById(1L);
        System.out.println("menu="+menu);
    }
}
