package com.jt;

import com.jt.pojo.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.Map;

@SpringBootTest
public class MyRedisTemplateTests {
    @Autowired
    private RedisTemplate redisTemplate;
    @Test
    void testDataOper02(){
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.put("blog02","id",100L);
        hashOperations.put("blog02","title","hello redis");
        Map blog02 = hashOperations.entries("blog02");
        System.out.println(blog02);
    }
    @Test
    void testDataOper01(){
      Blog blog=new Blog();
      blog.setId(100L);
      //blog.setTitle("Redis Template test");

      ValueOperations valueOperations =
                redisTemplate.opsForValue();
      valueOperations.set("blog01", blog);//value会采用json方式序列化
      blog=(Blog)valueOperations.get("blog01");//反序列化
    };
}
