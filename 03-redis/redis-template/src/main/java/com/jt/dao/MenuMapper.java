package com.jt.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.Menu;
import org.apache.ibatis.annotations.Mapper;

/**
 * 基于此对象操作关系数据库中的menu信息
 */
@Mapper
public interface MenuMapper
        extends BaseMapper<Menu> {
}
