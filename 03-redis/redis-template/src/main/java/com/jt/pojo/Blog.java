package com.jt.pojo;

import java.io.Serializable;

public  class Blog implements Serializable {
    private static final long serialVersionUID = 4343493523013416771L;
    private Long id;
    private String title;
    //.....

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 假如对对象执行json方式的序列化,
     * 默认会将get方法中get单词后面的名字作为key(首字母小写),
     * get方法的返回值作为value,存储到json字符串
     * @return
     */
    private String getTitle() {
        return title;
    }

    /**
     * json反序列化时,它会基于json字符串中key找当前
     * 对象的set方法,进行数据初始化
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
