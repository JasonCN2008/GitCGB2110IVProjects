package com.jt.service;
import com.jt.pojo.Menu;
/**
 * 菜单业务接口
 */
public interface MenuService {
    /**
     * 基于菜单id查询菜单信息
     * @param id
     * @return
     */
    Menu selectById(Long id);

    /**
     * 添加新的menu对象
     * @param menu
     * @return
     */
    Menu insertMenu(Menu menu);

    /**
     * 更新menu对象
     * @param menu
     * @return
     */
    Menu updateMenu(Menu menu);
}
