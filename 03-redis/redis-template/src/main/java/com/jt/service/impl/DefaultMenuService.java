package com.jt.service.impl;

import com.jt.dao.MenuMapper;
import com.jt.pojo.Menu;
import com.jt.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class DefaultMenuService implements MenuService {
    @Autowired
    private MenuMapper menuMapper;

    /** @Cacheable注解描述的方法为一个缓存切入点方法,
     * 用于告诉系统底层,将方法返回值写入到缓存中*/
    @Cacheable(value = "menuCache",key = "#id")
    @Override
    public Menu selectById(Long id) {
        return menuMapper.selectById(id);
    }

    /**CachePut注解描述的方法为一个缓存切入点方法,
     * 用于告诉系统底层,将方法返回值添加到缓存中
     * 默认key的名字为"value属性值::key",
     * 例如menuCache::5
     * */
    @CachePut(value = "menuCache",key="#menu.id")
    @Override
    public Menu insertMenu(Menu menu) {
         menuMapper.insert(menu);
         return menu;
    }

    @CachePut(value = "menuCache",key="#menu.id")
    @Override
    public Menu updateMenu(Menu menu) {
        menuMapper.updateById(menu);
        return menu;
    }
}
