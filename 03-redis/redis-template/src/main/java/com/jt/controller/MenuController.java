package com.jt.controller;

import com.jt.pojo.Menu;
import com.jt.service.MenuService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/menu")
public class MenuController {

    //@Autowired
    //@Qualifier("defaultMenuService")
    //private MenuService menuService;

    private MenuService menuService;
    //@Autowired //只有一个构造方法时,@Autowired这个注解可以省略
    public MenuController(
            @Qualifier("defaultMenuService") MenuService menuService){
        this.menuService=menuService;
    }

    @GetMapping("/{id}")
    public Menu doSelectById(@PathVariable("id") Long id){
        return menuService.selectById(id);
    }
    @PostMapping
    public String doInsertMenu(@RequestBody Menu menu){
        menuService.insertMenu(menu);
        return "insert ok";
    }
    @PutMapping
    public String doUpdateMenu(@RequestBody Menu menu){
        menuService.updateMenu(menu);
        return "update ok";
    }

}
