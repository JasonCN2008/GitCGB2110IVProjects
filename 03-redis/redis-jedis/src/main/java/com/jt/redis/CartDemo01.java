package com.jt.redis;

import redis.clients.jedis.Jedis;

import java.util.Map;

public class CartDemo01 {
    /**
     * 将商品添加到购物车
     * @param userId
     * @param goodsId
     * @param num
     */
    static void addCart(Long userId,Long goodsId,int num){
        //1.建立与redis的连接
        Jedis jedis = JedisDataSource.getConnection();
        //2.执行商品添加操作
        jedis.hincrBy("cart:"+userId, String.valueOf(goodsId), num);
        //3.释放资源
        jedis.close();
    }

    /**
     * 查看购物车商品
     * @param userId
     * @return
     */
    static Map<String,String> listCart(Long userId){
        //1.建立与redis的连接
        Jedis jedis = JedisDataSource.getConnection();
        //2.执行商品查询操作
        Map<String, String> map = jedis.hgetAll("cart:" + userId);
        //3.释放资源
        jedis.close();
        return map;
    }
    public static void main(String[] args) {
        //1.定义商品(用户商品id进行标识)
        long goods01=201L; //标识商品id
        long goods02=202L;
        long goods03=203L;
        //2.定义用户(登录用户)
        long userId=1001L; //(这里标识登录用户id)
        //3.执行购物操作(将商品信息写入到购物车)
        addCart(1001L,goods01,1);
        addCart(1001L,goods02,2);
        addCart(1001L,goods03,3);
        //4.查看购物车商品
        Map map=listCart(userId);
        System.out.println(map);
    }
}
