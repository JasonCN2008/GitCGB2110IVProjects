package com.jt.redis;

import redis.clients.jedis.Jedis;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 单点登录系统:基于redis存储用户登录状态
 * 1)完成用户身份的认证
 * 2)完成用于资源访问的授权
 * SSO单点登录系统实现?
 * 1)定义登录方法(认证方法),用户完成用户身份认证.
 * 2)用户认证成功,将用户信息存储到redis数据库,并返回一个令牌(UUID)
 * 3)定义一个资源方法,用户可以携带令牌访问资源.
 */
public class SSODemo01 {
    static String doLogin(String username,String password){
        //1.判定参数的合法性
        if(username==null||"".equals(username))
            throw new IllegalArgumentException("username can not be null");
        //2.基于用户名查找用户(将来要查询数据库)
        if(!"jack".equals(username))
            throw new RuntimeException("user is not exits");
        //3.比对密码是否正确(将来是密码加密后和数据库中查询到的已加密的密码进行比对)
        if(!"123456".equals(password))
            throw new RuntimeException("password is error");
        //4.将用户信息存储redis
        Jedis jedis = JedisDataSource.getConnection();
        String token= UUID.randomUUID().toString();//随机字符串
        jedis.hset(token,"username",username);
        jedis.hset(token, "permissions", "sys:res:select,sys:res:create");
        jedis.expire(token, 1);
        jedis.close();
        //5.返回一个令牌
        return token;
    }
    static Object doGetResource(String token){
        //1.校验参数有效性
        if(token==null||"".equals(token))
            throw new IllegalArgumentException("please login");
        //2.基于token从redis查询用户信息
        Jedis jedis = JedisDataSource.getConnection();
        Map<String, String> userMap = jedis.hgetAll(token);
        jedis.close();
        //3.判定用户是否已经登录
        if(userMap==null||userMap.size()==0)
            throw new RuntimeException("login timeout");
        //4.判定用户是否有访问这个方法的权限
        String permissionStr=userMap.get("permissions");
        String[] permissionArray = permissionStr.split(",");
        List<String> permissions = Arrays.asList(permissionArray);
        if(!permissions.contains("sys:res:select"))
            throw new RuntimeException("No Permission");
        //5.返回要访问的资源
        return "Your Resource";
    }
    public static void main(String[] args) throws InterruptedException {
        //1.定义用户
        String username="jack";
        String password="123456";
        //2.执行用户登录
        String token=doLogin(username,password);
        System.out.println(token);
        //3.携带令牌访问资源
        //Thread.sleep(2000);
        Object resource=doGetResource(token);
        System.out.println(resource);
    }
}
