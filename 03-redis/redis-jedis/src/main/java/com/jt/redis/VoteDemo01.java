package com.jt.redis;

import redis.clients.jedis.Jedis;

import java.util.Set;

/**
 * 需求:基于某个活动实现一个投票系统
 * 1)活动id
 * 2)基于活动id进行投票
 * 2.1)同一个用户只能投票一次
 * 2.2)假如已经投过,再投就是取消投票
 * 3)可以获取活动id的投票总数以及哪些用户参与过这个投票
 */
public class VoteDemo01 {
    /**
     * 基于活动id执行投票操作
     * @param activityId 活动id
     * @param userId 用户id
     * @return 是否投票成功
     */
    static boolean doVote(String activityId,String userId){
        //1.参数校验
        //2.执行投票操作
        //2.1连接redis
        Jedis jedis = JedisDataSource.getConnection();
        //2.2检查是否投过票,是第一次则向集合中添加用户id
        Boolean flag = jedis.sismember(activityId, userId);
        if(flag){//假如已投过票,则取消投票
            jedis.srem(activityId, userId);
            jedis.close();
            return false;
        }
        //第一次投票,直接将用户id添加到redis的set集合中.
        jedis.sadd(activityId, userId);
        jedis.close();
        return true;
    }

    /**
     * 获取总票数
     * @param activityId
     * @return
     */
    static Long doCount(String activityId){
        Jedis jedis = JedisDataSource.getConnection();
        Long scard = jedis.scard(activityId);
        jedis.close();
        return scard;
    }
    /**
     * 获取参与投票的用户id
     * @param activityId
     * @return
     */
    static Set<String> doGetUsers(String activityId){
        Jedis jedis = JedisDataSource.getConnection();
        Set<String> smembers = jedis.smembers(activityId);
        jedis.close();
        return smembers;
    }
    public static void main(String[] args) {
        //1.定义活动
        String activityId="1001";
        //2.定义参与活动的用户(这里的用户将来是登录用户)
        String user1="101";
        String user2="102";
        String user3="103";
        //3.执行用户投票动作
        boolean flag=doVote(activityId,user1);//第一次投票,返回值为true
        flag=doVote(activityId,user2);
        flag=doVote(activityId,user3);
        flag=doVote(activityId,user1);//取消投票
        //4.获取活动的总票数
        long count=doCount(activityId);
        System.out.println(count);
        //5.获取哪些用户参与了这个投票活动
        Set<String> users=doGetUsers(activityId);
        System.out.println(users);
    }

}
