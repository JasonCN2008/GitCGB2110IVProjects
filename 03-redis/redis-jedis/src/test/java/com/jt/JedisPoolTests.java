package com.jt;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class JedisPoolTests {
      @Test
      public void testJedisPool(){
         //1.创建Jedis连接池(池的类型为JedisPool)
          GenericObjectPoolConfig config=new GenericObjectPoolConfig();
          config.setMaxTotal(16);//最大连接数
          config.setMaxIdle(8);//最大空闲连接数
          config.setMinIdle(4);//最小空闲连接数

          JedisPool jedisPool=
                  //new JedisPool("192.168.126.129",6379);
                  new JedisPool(config,"192.168.126.129",6379);

         //2.从池中获取一个连接
          Jedis resource = jedisPool.getResource();
          //3.读写数据
          resource.set("id", "1000");
          String id = resource.get("id");
          System.out.println(id);
         //4.释放资源
          resource.close();//不是关连接,而是将连接放回池中.
          jedisPool.close();//关闭池,实际应用中这个池会在服务关闭时去关.
      }
}
