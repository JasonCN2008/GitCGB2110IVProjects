package com.jt;

import org.junit.Test;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

public class JedisClusterTests {

    @Test
    public void testCluster(){
        //1.构建集群对象
        Set<HostAndPort> nodes=new HashSet<>();
        nodes.add(new HostAndPort("192.168.126.129",8010));
        nodes.add(new HostAndPort("192.168.126.129",8011));
        nodes.add(new HostAndPort("192.168.126.129",8012));
        nodes.add(new HostAndPort("192.168.126.129",8013));
        nodes.add(new HostAndPort("192.168.126.129",8014));
        nodes.add(new HostAndPort("192.168.126.129",8015));
        JedisCluster jedisCluster=new JedisCluster(nodes);
        //2.读写数据
        jedisCluster.set("xyz", "100");
        String xyz = jedisCluster.get("xyz");
        System.out.println(xyz);
        //3.释放资源
        jedisCluster.close();
    }
}
