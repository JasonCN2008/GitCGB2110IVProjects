package com.jt;

import com.google.gson.Gson;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.*;

public class JedisTests {

    @Test
    public void testSetOper01() {
        //1.连接redis
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        //2.添加数据
        jedis.sadd("count", "1", "1", "2");
        //3.取出数据
        //3.1判定集合中是否有某个元素
        Boolean flag = jedis.sismember("count", "2");
        //3.2集合中元素的个数
        Long size = jedis.scard("count");
        System.out.println(size);
        //3.3集合中所有的元素
        Set<String> set = jedis.smembers("count");
        System.out.println(set);
        //4.释放资源
        jedis.close();
    }

    /**
     * 测试list数据类型
     */
    @Test
    public void testListOper01(){
        //1.创建连接(这个Jedis连接对象不能多线程共享)
        Jedis jedis=new Jedis("192.168.126.129", 6379);
        //2.数据读写测试
        //2.1先进先出队列
        jedis.lpush("lst1", "A","B","B","C");
        List<String> lst1 = jedis.rpop("lst1", jedis.llen("lst1").intValue());
        System.out.println("lst1="+lst1);
        //2.2先进后出栈
        jedis.lpush("lst2", "A","B","C","D");
        List<String> lst2= jedis.lpop("lst2", jedis.llen("lst2").intValue());
        System.out.println(lst2);
        //2.3阻塞式队列
        jedis.lpush("lst3", "A","B","C");
        List<String> lst3 = jedis.rpop("lst3", jedis.llen("lst3").intValue());
        jedis.brpop(50, "lst3");//50为阻塞时间,没有内容了会阻塞
        System.out.println("lst3="+lst3);
        //3.释放资源
        jedis.close();
    }

    /**
     * 测试hash数据类型操作
     */
    @Test
    public void testHashOper01(){
        //1.创建连接(这个Jedis连接对象不能多线程共享)
        Jedis jedis=new Jedis("192.168.126.129", 6379);
        //2.读写redis中hash数据类型
        //2.1存储数据
        jedis.hset("point1", "x", "100");
        jedis.hset("point1", "y", "200");
        jedis.hset("point1", "z", "300");
        jedis.expire("point1", 10);//设置key有效时长
        //2.2修改数据
        jedis.hset("point1", "y", "300");
        jedis.hincrBy("point1", "y", 10);
        //2.3判定key是否存在
        Boolean hexists = jedis.hexists("point1", "y");//判定小key是否存在
        Boolean exists=jedis.exists("point1");//判定大key是否存在(适合所有类型)
        //2.4删除key
        Long hdel = jedis.hdel("point1", "z");//删除小key
        Long del=jedis.del("point1");//删除大key(适合所有类型)
        //2.5取数据
        //jedis.hkeys("point1");
        //jedis.hvals("point1");
        jedis.hget("point1", "x");
        Map<String, String> point1 = jedis.hgetAll("point1");
        System.out.println(point1);
        //3.释放资源
        jedis.close();
    }
    /**
     * 基于gson中的API对象,将一个Map转换为Json格式字符串,
     * 然后将其存储到redis,并在存储时,指定key的有效期,
     * 然后再将redis中的json数据读出,再转换为map对象,进行输出.
     */
    @Test
    public void testStringOper02(){
        //1.创建连接(这个Jedis连接对象不能多线程共享)
        Jedis jedis=new Jedis("192.168.126.129", 6379);
        //2.读写数据
        //2.1构建一个map对象
        Map<String,String> userMap=new HashMap<>();
        userMap.put("id", "200");
        userMap.put("username", "Jack");
        //2.2将map对象转换为json字符串
        Gson gson=new Gson();
        String jsonStr=gson.toJson(userMap);
        //2.3存储字符串
        String token= UUID.randomUUID().toString();
        jedis.setex(token,10,jsonStr);//存储数据时执行有效期
        //2.4读取字符串
        jsonStr=jedis.get(token);
        System.out.println(jsonStr);
        //2.5将字符串转换为map (为什么?方便操作数据)
        Map map = gson.fromJson(jsonStr, Map.class);
        System.out.println(map);
        //3.释放资源
        jedis.close();
    }
    /**
     * 测试字符串操作
     */
    @Test
    public void testStringOper01(){
       //1.创建连接(这个Jedis连接对象不能多线程共享)
        Jedis jedis=new Jedis("192.168.126.129", 6379);
       //2.读写reddis中String类型数据
       //2.1添加数据
        //jedis.set("id", "100");
        jedis.set("name","tony");
        jedis.set("phone", "11111");
       //2.2修改数据
        jedis.incr("id");//递增
        jedis.set("name", "mike");//key相同值覆盖
        jedis.expire("name", 1);//设置有效时长
        jedis.append("addr", "北京");//追加,key不存在则创建
       //2.3删除数据
        jedis.del("phone");
       //2.4查询数据
        //jedis.get("name");只取一个key的值
        List<String> mget = jedis.mget("id", "name", "phone","addr");
        System.out.println(mget);
        //3.释放资源
        jedis.close();
    }

    @Test
    public void testGetConnection(){
        //1.创建jedis对象(此对象相当于一个连接对象,可以通过此对象操作redis)
        Jedis jedis=new Jedis("192.168.126.129", 6379);
        //2.测试ping操作
        String result = jedis.ping();
        System.out.println(result);
        //3.释放资源
        jedis.close();
    }
}
