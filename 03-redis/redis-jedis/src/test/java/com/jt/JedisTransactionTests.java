package com.jt;


import com.jt.redis.JedisDataSource;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.util.List;

public class JedisTransactionTests {

    @Test
    public void testTransaction(){
        //1.建立连接
        Jedis jedis= JedisDataSource.getConnection();
        //2.基于事务的读写操作(模拟转账操作)
        jedis.set("tony","100");
        jedis.set("jack","800");
        //2.1开启事务
        Transaction multi = jedis.multi();
        try {
            multi.incrBy("tony", 200);
            multi.decrBy("jack", 200);
            int n = 100, b = 0;
            n /= b;//此时会有异常 (模拟异常后取消事务)
            //2.2提交事务
            List<Object> exec = multi.exec();
            System.out.println(exec);
        }catch (Exception e){
            e.printStackTrace();
            //2.2取消事务
            multi.discard();
        }
        //3.释放资源
        jedis.close();
    }
}
